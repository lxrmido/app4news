var _widgets = [
    'View',
    'ViewMask',
    'Head',
    'Menu',
    'MenuItem',
    'Channel',
    'ChannelList'
];
var _contents = [
    'picset',
    'textlist'
];

var _paths = {
    'yago'    : 'lib/yago',
    'ynet'    : 'lib/ynet',
    'a4n'     : 'lib/a4n',
    'widget'  : 'lib/widget',
    'content' : 'lib/content'
};

!(function(){
    var i;
    for(i = 0; i < _widgets.length; i ++){
        _paths['widget' + _widgets[i]] = 'widget/' + _widgets[i];
    }
    for(i = 0; i < _contents.length; i ++){
        _paths['content_' + _contents[i]] = 'content/' + _contents[i];
    }
})();

requirejs.config({
    baseUrl: 'js/',
    paths: _paths,
    urlArgs: 'bust=' + new Date().getTime()
});

requirejs(['config', 'yago', 'ynet', 'a4n'], function(config, Yago, N, app){
    
    
    var _d = app.initNodes(document.body)
    
    // 显示 ViewMain
    app.show(_d.dwViewMain);
    
    // 附加轻按事件到ViewMain标题栏的左侧按钮（菜单按钮）
    app.tap(_d.dwViewMain.dwHead.dwBtnLeft, function(){
        if(_d.dwViewMain.dwLeftMenu.isShow){
            hideLeftMenu();
        }else{
            showLeftMenu();
        }
    });
    
    // 初始化频道栏
    N('appf.channel_list', function(d){
        _d.dwViewMain.dwChannel.setChannels(d.args.channel_list);
        _d.dwViewMain.dwChannel.setDisplayedChannels(['index', 'news', 'notice', 'cuxiao', 'media']);
    });
    _d.dwViewMain.dwChannel.linkList(_d.dwViewMain.dwChannelList);
    
    
    
//    app.show(_d.dwViewArticle);
    
    
    function showLeftMenu(){
        _d.dwViewMain.dwLeftMenu.isShow = true;
        app.show(_d.dwViewMain.dwLeftMenu, {
            mask : _d.dwViewMain.dwMask,
            maskcallback : hideLeftMenu
        });
    }
    
    function hideLeftMenu(){
        _d.dwViewMain.dwLeftMenu.isShow = false;
        app.hide(_d.dwViewMain.dwLeftMenu, {
            mask : _d.dwViewMain.dwMask
        });
    }
})