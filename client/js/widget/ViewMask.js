define(['config', 'yago'], function(config, Yago){
    
    var ttAName = Yago.transitionAttrName;
    var tfAName = Yago.transformAttrName;
    
    var ViewMask = {
        init : function(d){
            d.maskCallback = null;
            d.addEventListener('touchstart', function(){
                if(this.maskCallback){
                    this.maskCallback();
                    this.maskCallback = null;
                }
                return false;
            });
            d.attachCallback = function(cb){
                this.maskCallback = cb;
            }
        },
        
        show : function(n, option){
            n.attachCallback(option.maskcallback);
            n.style.opacity = 0;
            n.style.display = 'block';
            n.style[ttAName] = 'opacity ' + option.transition + ' ' + option.time + 'ms';
            setTimeout(function(){
                n.style.opacity = 1;
                setTimeout(function(){
                    n.style.opacity = 1;
                    n.style[ttAName] = '';
                }, option.time);
            }, 10);
        },
        
        hide : function(n, option){
            n.style[ttAName] = '';
            n.style.opacity  = 1;
            n.style[ttAName] = 'opacity ' + option.transition + ' ' + option.time + 'ms';
            setTimeout(function(){
                n.style.opacity = 0;
                setTimeout(function(){
                    n.style.display  = 'none';
                    n.style[ttAName] = '';
                }, option.time);
            }, 10);
        }
    };
    
    return ViewMask;
});