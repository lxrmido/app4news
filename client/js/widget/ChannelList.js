define(['config', 'content', 'yago', 'ynet'], function(config, content, Yago, N){

    var a4n;
    
    var ttAName = Yago.transitionAttrName;
    var tfAName = Yago.transformAttrName;
    
    var layout_height;
    var layout_map;
    
    function mkdiv(className){
        var d = document.createElement('div');
        d.className = className;
        return d;
    }
    
    var ChannelList = {
        ready : function(a){
            a4n = a;
        },
        
        init : function(d){
            
            var cs   = mkdiv('apf-channel-list-cs');
            // 内容区容器
            var ct   = mkdiv('apf-channel-list-ct');
            // 用于动画效果
            var ci_l = mkdiv('apf-channel-list-ci left');
            var ci_r = mkdiv('apf-channel-list-ci right');
            // 中间内容区
            var ci_m = mkdiv('apf-channel-list-ci');
            // 下拉刷新组件
            var dp_r = mkdiv('apf-channel-list-refresh');
            
            var dp_r_i = mkdiv('apf-channel-list-refresh-icon');
            var dp_r_t = mkdiv('apf-channel-list-refresh-text');
            
            var sc = mkdiv('apf-scrollbar');
            
            var view = a4n.getView(d);
                        
            d.appendChild(cs);
            
            cs.appendChild(ci_l);
            cs.appendChild(ci_m);
            cs.appendChild(ci_r);
            
            ci_m.appendChild(ct);
            ci_m.appendChild(sc);
            ci_m.appendChild(dp_r);
            dp_r.appendChild(dp_r_i);
            dp_r.appendChild(dp_r_t);
            
            dp_r.style.left = parseInt((Yago.scrW - config.SIZE.DROP_REFRESH_WIDTH) / 2) + 'px';
            dp_r.style.opacity = 1;
            dp_r_t.innerHTML = config.TEXT.DROP_REFRESH_ING;
            dp_r_i.style.backgroundPositionX = - config.SIZE.DROP_REFRESH_STEP * 11 + 'px';
            
            Yago.touch.enableScroll(ct, 'y', {
                preventDefault   : true,
                scrollbar : sc,
                overstepCallback : function(y){
                    switch(true){
                        case y < 0:
                            dp_r.style.opacity = 0;
                            dp_r_i.style.backgroundPositionX = 0;
                            break;
                        case y < config.SIZE.DROP_REFRESH_THRESHOLD:
                            dp_r.style.opacity = (100 - config.SIZE.DROP_REFRESH_THRESHOLD + y) / 100;
                            dp_r_i.style.backgroundPositionX = - config.SIZE.DROP_REFRESH_STEP * Math.floor(y / config.SIZE.DROP_REFRESH_THRESHOLD * 11) + 'px';
                            dp_r_t.innerHTML = config.TEXT.DROP_REFRESH_DROPING;
                            Yago.css.moveY(dp_r, (y - config.SIZE.DROP_REFRESH_HEIGHT) / 2);
                            break;
                        default:
                            dp_r.style.opacity = 1;
                            dp_r_i.style.backgroundPositionX = - config.SIZE.DROP_REFRESH_STEP * 11 + 'px';
                            dp_r_t.innerHTML = config.TEXT.DROP_REFRESH_WAIT_RE;
                            break;
                    }
                },
                releaseCallback : function(y){
                    switch(true){
                        case y < 0:
                            break;
                        case y < config.SIZE.DROP_REFRESH_THRESHOLD:
                            break;
                        default:
                            dp_r_i.style.backgroundPositionX = - config.SIZE.DROP_REFRESH_STEP * 11 + 'px';
                            ct.style.top = config.SIZE.DROP_REFRESH_THRESHOLD + 'px';
                            d.apfLinkChannel && d.apfLinkChannel.refreshChannel();
                            dp_r_t.innerHTML = config.TEXT.DROP_REFRESH_ING;
                            break;
                    }
                }
            });
            
            d.apfClearContent  = function(){
                ct.innerHTML  = '';
            }
            
            d.apfUpdateContent = function(d, y){
                var cti;
                ct.style.top  = '0px';
                ct.innerHTML  = '';
                layout_height = 0;
                layout_map    = {};
                d.forEach(function(data){
                    if(!content.check(data.type)){
                        console.log('Unrecognized content: ' + data.type);
                        return;
                    }
                    cti = content.build(data.type, data, layout_height);
                    ct.appendChild(cti.element);
                    cti.callback && cti.callback();
                    layout_map[layout_height] = cti.element;
                    layout_height = cti.layout_height;
                });
                ct.mv.reWrap();
                ct.mv.offset = y || 0;
                Yago.css.moveY(ct, y);
            }
            
            var sliding = false;
            
            d.apfContent = ct;
            
            d.apfSlideToRight = function(needRefresh){
                d.apfSlideTo(-rect.width * 2, needRefresh);
            }
            d.apfSlideToLeft = function(needRefresh){
                d.apfSlideTo(0, needRefresh);
            }
            d.apfSlideTo = function(x, needRefresh){
                sliding = true;
                if(needRefresh){
                    dp_r.style.opacity = 1;
                    dp_r_i.style.backgroundPositionX = - config.SIZE.DROP_REFRESH_STEP * 11 + 'px';
                    dp_r_t.innerHTML = config.TEXT.DROP_REFRESH_ING;
                }else{
                    dp_r.style.opacity = 0;
                }
                
                cs.style[ttAName] = "all ease-in 300ms";
                ct.style.opacity = 0;
                ct.style[ttAName] = "opacity ease-in 300ms";
                
                setTimeout(function(){
                    Yago.css.moveX(cs, x);
                    setTimeout(function(){
                        cs.style[ttAName] = '';
                        Yago.css.moveX(cs, -rect.width);
                        
                        setTimeout(function(){
                            ct.style.opacity = 1;
                            setTimeout(function(){
                                ct.style[ttAName] = '';
                            }, 350);
                        }, 10);
                        sliding = false;
                    }, 350);
                }, 10);
            }
            
            
            var rect;
            
            function update(){
                rect = d.getBoundingClientRect();
                cs.style.width    = rect.width * 3 + 'px';
                cs.style.height   = rect.height + 'px';
                Yago.css.moveX(cs, -rect.width);
                
                ci_l.style.left   = 0;
                ci_l.style.width  = rect.width + 'px';
                ci_l.style.height = rect.height + 'px';
                
                ci_m.style.left   = rect.width + 'px';
                ci_m.style.width  = rect.width + 'px';
                ci_m.style.height = rect.height + 'px';
                
                ci_r.style.left   = rect.width * 2 + 'px';
                ci_r.style.width  = rect.width + 'px';
                ci_r.style.height = rect.height + 'px';
            
                ct.mv.reWrap();
            }
            view.apfNeedUpdate.push(update);
        }
    };

    return ChannelList;
});