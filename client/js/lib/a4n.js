/*
 * APPF模块
 * dev@doc.moe
 * lxrmido@lxrmido.com
 */
define(['config', 'yago', 'ynet', 'widget'], function(config, Yago, N, widget){
    
    var SVR = {
        BASE_URL  : '',
        JSONP_URL : ''
    };
    
    var APF = {};
    
    APF.setServerURL = function(url){
        if(url.charAt(url.length - 1) == '/'){
            SVR.BASE_URL = url;
        }else{
            SVR.BASE_URL = url + '/';
        }
        N.setURL(SVR.JSONP_URL = SVR.BASE_URL + 'method/jsonp.php');
    }
    APF.setServerURL(config.URL.SVR_URL);
       
    
    function apf_initNode(n){
        if(n.dataset.apf){
            var a = n.dataset.apf.split(',');
            n.apfType = a[0];
            a.forEach(function(x){
                widget.init(x, n);
            });
        }
    }
    function apf_initNodes(n){
        return Yago.initNodes(n, apf_initNode);
    }
    APF.initNodes = apf_initNodes;
    function apf_show(n, option){
        widget.show(n.apfType, n, option);
    }
    APF.show = apf_show;
    function apf_hide(n, option){
        widget.hide(n.apfType, n, option);
    }
    APF.hide = apf_hide;
    function apf_tap(n, func, subClass){
        Yago.touch.tap(n, 0, func, subClass, true);
    }
    APF.tap = apf_tap;
    function apf_getView(d){
        while(d.apfType != 'View'){
            d = d.parentNode;
        }
        return d;
    }
    APF.getView = apf_getView;
    
    widget.ready(APF);
    return APF;
    
});
