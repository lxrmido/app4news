define(
    _widgets.map(
        function(x){
            return 'widget' + x;
        }
    ), 
    function(){
        var widget = {
            
            contains : {},
            init : function(type, d){
                if(widget.contains[type]){
                    return widget.contains[type].init(d);
                }else{
                    console.log('Unrecognized widget: ' + type);
                    return false;
                }
            },
            show : function(type, n, option){
                return widget.contains[type].show(n, option);
            },
            hide : function(type, n, option){
                return widget.contains[type].hide(n, option);
            },
            ready : function(a){
                var i;
                for(i in widget.contains){
                    widget.contains[i].ready && widget.contains[i].ready(a);
                }
            }
        };
        var i;
        for(i = 0; i < _widgets.length; i ++){
            widget.contains[_widgets[i]] = arguments[i];
        }
        return widget;
    }
);