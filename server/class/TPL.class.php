<?php

class TPL{

    public static function show($tpl_name, $config = array(), $frame = 'common'){

        global $TPL_DEFAULT_JS;
        global $TPL_DEFAULT_CSS;

        $default_config = array(
            'title'       => TPL_DEFAULT_TITLE,
            'js'          => array(),
            'css'         => array(),
            'default_js'  => $TPL_DEFAULT_JS,
            'default_css' => $TPL_DEFAULT_CSS,
            'ext_js'      => array(),
            'ext_css'     => array(),
            'plugin'      => array()
        );

        foreach($default_config as $key => $value) {
            if(!isset($config[$key])){
                $config[$key] = $default_config[$key];
            }
        }

        # JS
        foreach ($config['default_js'] as $jsf) {
            $config['js'][] = $jsf;
        }
        $config['js'] = array_reverse($config['js']);

        # CSS
        foreach ($config['default_css'] as $csf) {
            $config['css'][] = $csf;
        }
        $config['css'] = array_reverse($config['css']);
        
        # Plugin
        foreach ($config['plugin'] as $plg) {
            $plugin_config = lx_plugin_config($plg);
            $config['ext_js']  = array_merge($config['ext_js'],  $plugin_config['js']);
            $config['ext_css'] = array_merge($config['ext_css'], $plugin_config['css']);
        }

        $s = new Smarty;

        global $_RG;

        $s->assign($config);
        $s->assign('_RG', $_RG);
        $s->assign('_RG_JSON', json_encode($_RG));

        $s->assign('tpl_name', $tpl_name . '.html');
        $s->display($frame . '.html');
        die();
    }
}