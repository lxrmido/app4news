<?php


class Channel{
    
    public static function parse($channel_name){
        include_once('conf/channel.php');
        if(!in_array($channel_name, $CHANNEL_AVAILABLE)){
            return FALSE;
        }
        $channel_parser = 'channel/'.$channel_name.'.php';
        if(!file_exists($channel_parser)){
            include_once('channel/fixed_demo.php');
        
            return ChannelParser::parse();
            return FALSE;
        }
        include_once($channel_parser);
        
        return ChannelParser::parse();
    }
}