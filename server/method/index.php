<?php
include '../global.php';

#是否以JSON形式输出
$METHOD['json'] = $JSON_IO = IO::r('json', 1, 'int') > 0;
# 方法名
$METHOD['name'] = IO::r('m');
# 获取实际文件路径
$METHOD['file'] = lx_method($METHOD['name']);
# 包含
include($METHOD['file']);