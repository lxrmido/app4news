<?php

$channel_list = array(
	'index'      => '精选',
	'news'       => '新闻动态',
	'notice'     => '最新公告',
	'cuxiao'     => '促销活动',
	'media'      => '媒体报道',
	'dishi'      => '地市动态',
	'enterprise' => '参与企业'
);

IO::O(array(
	'channel_list' => $channel_list
));