<?php

class ChannelParser{
    public static function parse(){
        return array(
            array(
                'type'   => 'picset',
                'hw'     => 0.62, 
                'items'  => array(
                    array(
                        'img'   => 'http://upload.ghwsx.gov.cn/2014/1230/1419929357702.jpg',
                        'title' => '11111',
                        'act'   => 'a1'
                    ),
                    array(
                        'img'   => 'http://upload.ghwsx.gov.cn/2014/1113/1415845336148.jpg',
                        'title' => '22222',
                        'act'   => 'a2'
                    ),
                    array(
                        'img'   => 'http://upload.ghwsx.gov.cn/2014/1113/1415845253604.jpg',
                        'title' => '3333',
                        'act'   => 'a3'
                    )
                )
            )
        );
    }
}