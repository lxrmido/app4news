<?php

class ChannelParser{
    public static function parse(){
        
        // 抓取原站点首页
        $page_data = Grab::from_url('http://www.ghwsx.gov.cn/');
        
        // 获取轮播图
        $picset = self::grab_picset($page_data);
        
        // 获取纯文本标题列表
        $textlist = self::grab_textlist($page_data);
        
        return array(
            $picset,
            $textlist
        );
    }
    
    public static function grab_picset($str){
        // 根据广货网上行的首页轮播图格式构造正则表达式，以获取其内容
        $pattern = "/<a href=\"([^\"]+)\">[^<]*<img src=\"([^\"]+)\">[^<]*<p>([^<]+)</";
        preg_match_all($pattern, $str, $result);

        $n   = count($result[1]);
        $items = array();
        // 构造图集信息
        for($i = 0; $i < $n; $i ++){
            $items[] = array(
                'img'   => $result[2][$i],
                'title' => $result[3][$i],
                'act'   => $result[1][$i]
            );
        }
        return array(
            'type'   => 'picset',
            // 图片高度与宽度的比例
            'hw'     => 0.62,
            'items'  => $items
        );
    }
    
    public static function grab_textlist($str){
        // 根据广货网上行的首页文章列表构造
        $pattern = "/<span[^>]+>([^<]+)<[^=]*=\"([^\"]+)\">([^<]+)</";
        preg_match_all($pattern, $str, $result);
        $n   = count($result[1]);
        $items = array();
        // 构造纯文本标题列表
        for($i = 0; $i < $n; $i ++){
            $items[] = array(
                'act'   => $result[2][$i],
                'title' => $result[3][$i],
                'date'  => $result[1][$i]
            );
        }
        return array(
            'type'   => 'textlist',
            // 图片高度与宽度的比例
            'items'  => $items
        );
    }
    
}