<?php

class ChannelParser{
	public static function parse(){

		// 抓取原站点首页
        $page_data = Grab::from_url('http://www.ghwsx.gov.cn/news/');
        
          
        // 获取纯文本标题列表
        $textlist = self::grab_textlist($page_data);
        
        return array(
            $textlist
        );
	}

	public static function grab_textlist($str){
		$pattern = "/em><a href=\"([^\"]+)\" title=\"([^\"]+)\"[^>]*>[^<]*<[^<]*<[^>]*>[^>]*>([^<]+)</";
		preg_match_all($pattern, $str, $result);
        $n   = count($result[1]);
        $items = array();
        // 构造纯文本标题列表
        for($i = 0; $i < $n; $i ++){
            $items[] = array(
                'act'   => $result[1][$i],
                'title' => $result[2][$i],
                'date'  => $result[3][$i]
            );
        }
        return array(
            'type'   => 'textlist',
            // 图片高度与宽度的比例
            'items'  => $items
        );
	}
}